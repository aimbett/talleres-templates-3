package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		//TODO
		try
		{
			Scanner sc = new Scanner(System.in);
			System.out.println("Ingrese la cantidad de jugadores: ");
			int cant = sc.nextInt();
			ArrayList<Jugador> lista= new ArrayList<Jugador>();
			for (int i =1 ; i <= cant;i++)
			{
				System.out.println("");
				System.out.println("Ingrese el nombre completo del jugador "+i+": ");
				String n1 = sc.next();
				System.out.println("Ingrese el simbolo a usar por el jugador "+i+": ");
				String s1 = sc.next();
				System.out.format("Bienvenido a Linea Cuatro %s su simbolo es %s ",n1, s1);
				System.out.println("");
				System.out.println("");
				lista.add(new Jugador(n1, s1));
			}
			System.out.println("Ingrese la cantidad de filas (Mayor a 4): ");
			int filas = sc.nextInt();

			System.out.println("Ingrese la cantidad de columnas (Mayor a 4): ");
			int columnas = sc.nextInt();

			juego = new LineaCuatro(lista, filas, columnas);

			juego();

		}
		catch (Exception e)
		{
			System.err.println("Error:" + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
		boolean termino = false;

		while (termino == false)
		{
			try
			{
				System.out.println("El turno es de :"+ juego.darAtacante());
				System.out.println("Ingrese la columna en la cual desea ingresar su ficha:");
				int columna = sc.nextInt();
				boolean jugada = false;

				jugada = juego.registrarJugada(columna);

				while (jugada==false)
				{
					System.out.println("Ingrese una columna válida");
					columna = sc.nextInt();
					jugada = juego.registrarJugada(columna);
				}


				imprimirTablero();
				termino = juego.fin();
			}
			catch (Exception e)
			{
				System.err.println("Error, ingrese un número");
			}
		}
		
		System.out.println("Fin del juego! ");
		System.out.println();
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		Scanner sc = new Scanner(System.in);
		ArrayList<Jugador> lista= new ArrayList<Jugador>();

		System.out.println("");
		System.out.println("Ingrese el nombre completo del jugador: ");
		String n1 = sc.next();
		System.out.println("Ingrese el simbolo a usar por el jugador: ");
		String s1 = sc.next();
		System.out.format("Bienvenido a Linea Cuatro %s su simbolo es %s ",n1, s1);
		System.out.println("");
		System.out.println("");
		lista.add(new Jugador(n1, s1));
		lista.add(new Jugador("Maquina", "@"));

		System.out.println("Ingrese la cantidad de filas (Mayor a 4): ");
		int filas = sc.nextInt();

		System.out.println("Ingrese la cantidad de columnas (Mayor a 4): ");
		int columnas = sc.nextInt();

		juego = new LineaCuatro(lista, filas, columnas);

		juegoMaquina();
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
		boolean termino = false;

		while (termino == false)
		{
			try
			{
				System.out.println("El turno es de :"+ juego.darAtacante());
				System.out.println("Ingrese la columna en la cual desea ingresar su ficha:");
				int columna = sc.nextInt();
				boolean jugada = false;

				jugada = juego.registrarJugada(columna);

				while (jugada==false)
				{
					System.out.println("Ingrese una columna válida");
					columna = sc.nextInt();
					jugada = juego.registrarJugada(columna);
				}


				imprimirTablero();
				termino = juego.fin();

				if(termino==false)
				{
					System.out.println("El turno es de la maquina");
					juego.registrarJugadaAleatoria();

					imprimirTablero();
					termino = juego.fin();

				}

			}

			catch (Exception e)
			{
				System.err.println("Error, ingrese un número");
			}
		}
		System.out.println("Fin del juego! ");
		System.out.println();
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		String[][] tablero = juego.darTablero();
		for (int i=0; i< tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				System.out.print("|"+tablero[i][j]);
			}
			System.out.println("|");
		}
	}
}
