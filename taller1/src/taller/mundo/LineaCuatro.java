package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		boolean sinRegistrar = false;
		

		while (sinRegistrar ==false)
		{
			Random rn = new Random();
			 int r = rn.nextInt((tablero[0].length-1) - 0 + 1) + 0;
			for(int i = tablero.length-1; i>=0 && !sinRegistrar;i--)
			{
				if(tablero[i][r].equals("___"))
				{
					sinRegistrar = true;
					tablero[i][r] = "_@_";
					finJuego = terminar(i,r);
					turno = 0;
					atacante = jugadores.get(turno).darNombre();
				}
			}
		}


	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		boolean respuesta = false;
		for(int i = tablero.length-1; i>=0 && !respuesta;i--)
		{
			if(tablero[i][col].equals("___"))
			{
				respuesta = true;
				tablero[i][col] = "_"+jugadores.get(turno).darSimbolo()+"_";

				finJuego = terminar(i,col);

				if (turno != jugadores.size()-1)
				{
					turno ++;
				}
				else
				{
					turno = 0;
				}
			}
		}
		
		atacante = jugadores.get(turno).darNombre();
		return respuesta;
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		boolean termina = false;

		String simboloActual = jugadores.get(turno).darSimbolo();
		// Revisión Vertical




		int contVertical = 0;
		for(int i = fil+3;i>=fil;i--)
		{
			if(i>=0&&i<tablero.length)
			{
				if(tablero[i][col].equals("_"+simboloActual+"_")){
					contVertical++;
				}
			}
		}

		if (contVertical>=4)
		{
			termina = true;
		}



		for(int i=col-3;i<=col;i++)
		{
			int contHorizontal = 0;
			for(int j=i;j<=i+3;j++)
			{
				if(j>=0&&j<tablero[0].length)
				{
					if(tablero[fil][j].equals("_"+simboloActual+"_"))
					{
						contHorizontal++;
					}
				}
			}
			if(contHorizontal>=4)
			{
				termina =true;
			}

		}

		//Diagonal de Arriba a abajo
		for(int i=fil-3;i<=fil;i++)
		{
			for(int j =col-3;j<=col;j++)
			{
				int contDiagonal1=0;
				for(int i2=i;i2<=i+3;i2++)
				{
					for(int j2=j;j2<=j+3;j2++)
					{
						if(i2>=0&&j2>=0&&i2<tablero.length&&j2<tablero[0].length)
						{
							if(tablero[i2][j2].equals("_"+simboloActual+"_"))
							{
								contDiagonal1 ++;
							}
						}
					}
				}
				if (contDiagonal1>=4)
				{
					termina = true;
				}
			}
		}
		//Diagonal de Abajo a arriba
				for(int i=fil+3;i>=fil;i--)
				{
					for(int j =col-3;j<=col;j++)
					{
						int contDiagonal2=0;
						for(int i2=i;i2>=i+3;i2--)
						{
							for(int j2=j;j2<=j+3;j2++)
							{
								if(i2>=0&&j2>=0&&i2<tablero.length&&j2<tablero[0].length)
								{
									if(tablero[i2][j2].equals("_"+simboloActual+"_"))
									{
										contDiagonal2 ++;
									}
								}
							}
						}
						if (contDiagonal2>=4)
						{
							termina = true;
						}
					}
				}




		return termina;
	}



}
